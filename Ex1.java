import java.util.Scanner;

public class Ex1 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int height[] = new int[n];
        int highest = 0;
        for(int height_i=0; height_i < n; height_i++){
            height[height_i] = input.nextInt();
            if(height[height_i] > highest){
                highest = height[height_i];
            }
        }
        int total = 0;
        for(int i =0;i<n;i++){
            if(highest == height[i])
                total++;
        }
        System.out.println(total);

    }
}